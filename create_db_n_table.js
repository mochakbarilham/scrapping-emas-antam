var mysql = require('mysql');
require('dotenv').config();

console.log('Creating database '+process.env.DATABASE_NAME+"...")

var connection = mysql.createConnection({
    host: process.env.DATABASE_HOST,
    user: process.env.DATABASE_USERNAME,
    password: process.env.DATABASE_PASSWORD
});

connection.connect(function(err){
    if(err) {
        console.log(err);
    }
});

var database = "CREATE DATABASE " + process.env.DATABASE_NAME;
connection.query(database, function(err){
    if(err){
      console.log(err);
    } else {
      console.log("Database "+process.env.DATABASE_NAME+" created");
    }
});

connection.end(function(err){
    if(err) {
        console.log(err);
     }
    else {
        createTable();
    }
});

function createTable(){

    var conn = mysql.createConnection({
        host: process.env.DATABASE_HOST,
        user: process.env.DATABASE_USERNAME,
        password: process.env.DATABASE_PASSWORD,
        database: process.env.DATABASE_NAME
    });

    conn.connect(function(err){
        if(err) {
            console.log(err);
        }
    });
    
    console.log('Creating table...')
    var table = `CREATE TABLE harga_emas
    (
        id int(11) NOT NULL AUTO_INCREMENT,
        eb_05 double, eb_1 double,
        eb_2 double, eb_3 double,
        eb_5 double, eb_10 double,
        eb_25 double, eb_50 double,
        eb_100 double, eb_250 double,
        eb_500 double, eb_1000 double,
        ebgs_05 double, ebgs_1 double,
        ebb_10 double, ebb_20 double,
        periode_harga TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (id)
    )`;
    
    conn.query(table, function(err){
        if(err){
          console.log(err);
        } else {
          console.log("Database "+process.env.DATABASE_NAME+" created");
        }
    });
    
    conn.end(function(err){
        if(err) {
            console.log(err);
         }
    });
    
}