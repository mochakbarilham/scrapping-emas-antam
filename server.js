const express = require('express')
const app = express()
const port = 4761

var mysql = require('mysql');
require('dotenv').config({ path: __dirname + '/.env' });


var connection = mysql.createConnection({
    host: process.env.DATABASE_HOST,
    user: process.env.DATABASE_USERNAME,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME
});

app.get('/', (req, res) => {
    connection.query('SELECT * FROM harga_emas', function (error, rows, fields){
        if(error){
            console.log(error)
        } else{
            var data = {
                'status': 200,
                'values': rows
            };
            res.json(data);
            res.end();
        }
    });
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))