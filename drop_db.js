var mysql = require('mysql');
require('dotenv').config();


var connection = mysql.createConnection({
    host: process.env.DATABASE_HOST,
    user: process.env.DATABASE_USERNAME,
    password: process.env.DATABASE_PASSWORD
});

//Membuka koneksi ke database MySQL
connection.connect(function(err){
    if(err) {
        console.log(err);
    }
});

console.log('Dropping database '+process.env.DATABASE_NAME+'...')
var sql = "DROP DATABASE "+ process.env.DATABASE_NAME;
connection.query(sql, function(err){
    if(err){
      console.log(err);
    } else {
      console.log("Database dropped");
    }
});

//Menutup koneksi
connection.end(function(err){
    if(err) {
        console.log(err);
     }
});