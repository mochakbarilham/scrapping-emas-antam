const scrapeIt = require('scrape-it');
const scrapeTable = require('table-scraper');
const loadJsonFile = require('load-json-file');
const writeJsonFile = require('write-json-file');
const moment = require('moment');
const cron = require('node-cron');

var mysql = require('mysql');
require('dotenv').config({ path: __dirname + '/.env' });


var connection = mysql.createConnection({
    host: process.env.DATABASE_HOST,
    user: process.env.DATABASE_USERNAME,
    password: process.env.DATABASE_PASSWORD,
    database: process.env.DATABASE_NAME
});

function goldScrapper() {
    scrapeTable.get('https://www.logammulia.com/id/harga-emas-hari-ini')
    .then(function (tableData){
        connection.query(`INSERT INTO harga_emas 
        (eb_05, eb_1, eb_2, eb_3, eb_5, eb_10, eb_25, eb_50, eb_100, eb_250, eb_500, eb_1000, 
            ebb_10, ebb_20, ebgs_05, ebgs_1)
        values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)`, 
        [
            tableData[0][0]["Harga Dasar"].replace(/,/g, ''),
            tableData[0][1]["Harga Dasar"].replace(/,/g, ''),
            tableData[0][2]["Harga Dasar"].replace(/,/g, ''),
            tableData[0][3]["Harga Dasar"].replace(/,/g, ''),
            tableData[0][4]["Harga Dasar"].replace(/,/g, ''),
            tableData[0][5]["Harga Dasar"].replace(/,/g, ''),
            tableData[0][6]["Harga Dasar"].replace(/,/g, ''),
            tableData[0][7]["Harga Dasar"].replace(/,/g, ''),
            tableData[0][8]["Harga Dasar"].replace(/,/g, ''),
            tableData[0][9]["Harga Dasar"].replace(/,/g, ''),
            tableData[0][10]["Harga Dasar"].replace(/,/g, ''),
            tableData[0][11]["Harga Dasar"].replace(/,/g, ''),
            tableData[0][12]["Harga Dasar"].replace(/,/g, ''),
            tableData[0][13]["Harga Dasar"].replace(/,/g, ''),
            tableData[0][14]["Harga Dasar"].replace(/,/g, ''),
            tableData[0][15]["Harga Dasar"].replace(/,/g, ''),
            
        ],
        function (error, rows, fields){
            if (error) {
                console.log(error)
            }
            else {
                console.log('Scraping Success');
		process.exit();
            }
        })
    })
}

goldScrapper();
